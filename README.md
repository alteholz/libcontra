# libcontra
LIBrary for CONnection TRAcking

## dependencies

In order to build this package including documentation you need to

	apt install doxygen texlive-binaries texlive-latex-base texlive-latex-recommended
 	apt install pdflatex imagemagick cppcheck clazy

## building

In order to not interfere with the sources use an external build directory

	rm -rf build; mkdir build; cd build; cmake ..; make


## testing

	cd build; ctest -V; cd ..
	cd build; make test; cd ..

## usage

After building the library it is available in

	~/git/devel/libcontra/build/libcontra/src/libcontra.so

You can use it to track firefox by simply doing:

	export LD_PRELOAD=~/git/devel/libcontra/build/libcontra/src/libcontra.so
	firefox

Of course you won't see anything yet. There are other environment variables that control the amount of output you see:

	CONTRA_LOG_STDIO	true or yes / false or no
		default: false
		see output on stdio


	CONTRA_LOG_LEVEL	integer between 0 and 4
		default: 0

		0	show nothing	
		1	only show error messages
		2	show warning messages
		3	show info messages
		4	show debug messages


	CONTRA_LOG_FUNCTIONS	string = name of function to be logged
		default: nothing

		for example  "connect:socket"  creates output for all calls to connect() and socket()
		the value "all" creates output for all functions


	CONTRA_CONFIG_RESOLVE	 true or yes / false or no
		default: false

		log functions that are used during DNS access as well


	CONTRA_LOG_FILE		filename to write messages
		default: NIL

		write log messages to this file as well


At least I was a bit surprised to see the output of:

   CONTRA_LOG_STDIO=yes CONTRA_LOG_LEVEL=4 CONTRA_LOG_FUNCTIONS=connect CONTRA_LOG_FILE=/tmp/firefox.log CONTRA_CONFIG_RESOLVE=yes  firefox

