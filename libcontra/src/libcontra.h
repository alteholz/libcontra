/* 
  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program (see the file COPYING included with this
  distribution); if not, write to the Free Software Foundation, Inc.,
  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*!
*************************************************************************
*
* \file libcontra.h
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \todo documentation
*  the following wrapper functions need to be implemented:
*    sendto(), and recvfrom()
*    gethostbyname() and gethostbyaddr()
*    getaddrinfo() and freeaddrinfo()
*    select()
*
* \brief header file for libcontra
*
* for more info see documents in docu directory
*
*
* \addtogroup libcontra
* The group of the library.
* @{
************************************************************************/

#ifndef _LIBCONTRA_H_
#define _LIBCONTRA_H_

#ifndef RTLD_NEXT
/*! we need to define this in order to use RTLD_NEXT in dlopen() */
#  define _GNU_SOURCE
#endif

#include <dlfcn.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <pthread.h>

void libcontra_init();
char * libcontra_af2string(int family);
char * libcontra_sock2string(int type);
char * libcontra_oname2string(int optionName);

#endif
/*! @} */

