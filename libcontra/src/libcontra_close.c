/* 
  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program (see the file COPYING included with this
  distribution); if not, write to the Free Software Foundation, Inc.,
  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*!
*************************************************************************
*
* \file libcontra_close.c
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper for close()
*
* close a file descriptor
* man 2 close
*
* \addtogroup libcontra
* The group of the library.
* @{
************************************************************************/

#include <libcontra.h>
#include <libcontra_log.h>

extern bool	libcontra_logClose; /*!< this is needed to control the output in the logging function */
extern bool	libcontra_isInitialized; /*!< this is needed to control the output in the logging function */

/*!
*************************************************************************
*
* \fn int close(int socket)
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper around libc::close()
*
* @param[in] socket   socket to close
*
* \return On success, return 0
*         On error, return -1, errno is set
*
************************************************************************/
int close(int socket)
{
  int (*libc_close)(int socket);
  int rc;
  
  if (libcontra_isInitialized==false) {
    libcontra_init();
  } 

  if (libcontra_logClose) {
    libcontra_log(LOGLEVEL_DEBUG, "D: close start");
    libcontra_log(LOGLEVEL_INFO,  "I: close socket %i\n",socket);
  }

  *(void **)(&libc_close) = dlsym(RTLD_NEXT, "close");
  if(dlerror()) {
    errno = EACCES;
    return -1;
  }

  rc = (*libc_close)(socket);
  
  if (libcontra_logClose) {
    libcontra_log(LOGLEVEL_INFO, "I: close rc: %i", rc);
    libcontra_log(LOGLEVEL_DEBUG, "D: close end");
  }

  return rc;
}

/*! @} */

