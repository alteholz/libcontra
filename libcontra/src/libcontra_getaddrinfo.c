/* 
  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program (see the file COPYING included with this
  distribution); if not, write to the Free Software Foundation, Inc.,
  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*!
*************************************************************************
*
* \file libcontra_getaddrinfo.c
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper for getaddinfo()
*
* get addrinfo structures
* man 2 getaddrinfo
*
*
* \addtogroup libcontra
* The group of the library.
* @{
************************************************************************/

#include <libcontra.h>
#include <libcontra_log.h>
#include <netdb.h>

extern bool	libcontra_logGetaddrinfo; /*!< this is needed to control the output in the logging function */
extern bool	libcontra_isInitialized; /*!< this is needed to control the output in the logging function */

/*!
*************************************************************************
*
* \fn int getaddrinfo(const char *restrict nodename, const char *restrict servname, const struct addrinfo *restrict hints, struct addrinfo **restrict res);
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper around libc::getaddrinfo()
* 
* @param[in]   ai        pointer to addrinfo struct that shall be freed
*
* \return On success, return 0.
*         On error, an error code is returned (-> see man page or netdb.h)
*
************************************************************************/
int getaddrinfo(const char *restrict nodename, const char *restrict servname, const struct addrinfo *restrict hints, struct addrinfo **restrict res)
{
  int (*libc_getaddrinfo)(const char *restrict nodename, const char *restrict servname, const struct addrinfo *restrict hints, struct addrinfo **restrict res);
  int rc;

  if (libcontra_isInitialized==false) {
    libcontra_init();
  } 

  if (libcontra_logGetaddrinfo) {
    libcontra_log(LOGLEVEL_DEBUG, "D: getaddrinfo start");
    if (nodename!=NULL) libcontra_log(LOGLEVEL_INFO, "I: getaddrinfo: nodename: %s",nodename);
    if (servname!=NULL) libcontra_log(LOGLEVEL_INFO, "I: getaddrinfo: servname: %s",servname);
  }

  *(void **)(&libc_getaddrinfo) = dlsym(RTLD_NEXT, "getaddrinfo");
  if(dlerror()) {
    errno = EACCES;
    return -1;
  }

  rc=(*libc_getaddrinfo)(nodename, servname, hints, res);
  
  if (libcontra_logGetaddrinfo) {
    libcontra_log(LOGLEVEL_DEBUG, "D: getaddrinfo end");
  }

  return rc;
}

/*! @} */

