/* 
  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program (see the file COPYING included with this
  distribution); if not, write to the Free Software Foundation, Inc.,
  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*!
*************************************************************************
*
* \file libcontra_getsockopt.c
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper for getsockopt()
*
* get the socket options
* man 2 getsockopt
*
*
* \addtogroup libcontra
* The group of the library.
* @{
************************************************************************/

#include <libcontra.h>
#include <libcontra_log.h>

extern bool	libcontra_logGetsockopt; /*!< this is needed to control the output in the logging function */
extern bool	libcontra_isInitialized; /*!< this is needed to control the output in the logging function */

/*!
*************************************************************************
*
* \fn int getsockopt(int socket, int level, int option_name, void *restrict option_value, socklen_t *restrict option_len)
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper around libc::getsockopt()
*
* @param[in]  socket        get information about this socket
* @param[in]  level         protocol level at which the option resides
* @param[in]  option_name   retrieve value for this option
* @param[out] option_value  value of requested option
* @param[out] option_len    len of requested value
* 
* \return On success, return 0
*         On error, return -1, errno is set
* 
************************************************************************/
int getsockopt(int socket, int level, int option_name, void *restrict option_value, socklen_t *restrict option_len)
{
  int (*libc_getsockopt)(int socket, int level, int option_name, void *restrict option_value, socklen_t *restrict option_len);
  int rc;
  
  if (libcontra_isInitialized==false) {
    libcontra_init();
  } 

  if (libcontra_logGetsockopt) {
    libcontra_log(LOGLEVEL_DEBUG, "D: getsockopt start");
    libcontra_log(LOGLEVEL_INFO,  "I: getsockopt on socket v(%i) with level of %i -> %i %s v(%i)",socket, level, option_name, libcontra_oname2string(option_name), *option_len);
  }

  *(void **)(&libc_getsockopt) = dlsym(RTLD_NEXT, "getsockopt");
  if(dlerror()) {
    errno = EACCES;
    return -1;
  }

  rc = (*libc_getsockopt)(socket, level, option_name, option_value, option_len);
  
  if (libcontra_logGetsockopt) {
    libcontra_log(LOGLEVEL_INFO, "I: getsockopt rc: %i", rc);
    libcontra_log(LOGLEVEL_DEBUG, "D: getsockopt end");
  }

  return rc;
}

/*! @} */

