/* 
  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program (see the file COPYING included with this
  distribution); if not, write to the Free Software Foundation, Inc.,
  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*!
*************************************************************************
*
* \file libcontra_listen.c
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper for listen()
*
* listen for socket connections and limit the queue of incoming connections
* man 2 listen
*
*
* \addtogroup libcontra
* The group of the library.
* @{
************************************************************************/

#include <libcontra.h>
#include <libcontra_log.h>

extern bool	libcontra_logListen; /*!< this is needed to control the output in the logging function */
extern bool	libcontra_isInitialized; /*!< this is needed to control the output in the logging function */

/*!
*************************************************************************
*
* \fn int listen(int socket, int backlog)
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper around libc::listen()
* 
* @param[in]    socket  listen on this socket
* @param[in]    backlog limit the number of outstanding connections in the socket's listen queue
* 
* \return On success, return 0
*         On error, return -1, errno is set
*
* close a socket
*
************************************************************************/
int listen(int socket, int backlog)
{
  int (*libc_listen)(int socket, int backlog);
  int rc;
  
  if (libcontra_isInitialized==false) {
    libcontra_init();
  } 

  if (libcontra_logListen) {
    libcontra_log(LOGLEVEL_DEBUG, "D: listen start");
    libcontra_log(LOGLEVEL_INFO,  "I: listen on socket v(%i) with backlog of %i (out of %i)",socket, backlog, SOMAXCONN);
  }

  *(void **)(&libc_listen) = dlsym(RTLD_NEXT, "listen");
  if(dlerror()) {
    errno = EACCES;
    return -1;
  }

  rc = (*libc_listen)(socket, backlog);
  
  if (libcontra_logListen) {
    libcontra_log(LOGLEVEL_INFO, "I: listen rc: %i", rc);
    libcontra_log(LOGLEVEL_DEBUG, "D: listen end");
  }

  return rc;
}

/*! @} */

