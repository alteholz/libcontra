/* 
  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program (see the file COPYING included with this
  distribution); if not, write to the Free Software Foundation, Inc.,
  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*!
*************************************************************************
*
* \file libcontra_log.h
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \todo documentation
*
* \brief header file for libcontra_log functions
*
* for more info see documents in docu directory
*
*
* \addtogroup libcontra
* The group of the library.
* @{
************************************************************************/

#ifndef _LIBCONTRA_LOG_H_
#define _LIBCONTRA_LOG_H_

#include <ctype.h>


void _libcontra_log(int level, const char *fmt, ...)
    __attribute__((format (printf, 2, 3)));

/*! macro for logging function */
#define libcontra_log(level, fmt, ...) _libcontra_log(level, fmt"\n", ##__VA_ARGS__)

void libcontra_log_data(int level, const void *buffer, int length);
char * bool2str(bool value);

#define LOGLEVEL_NULL		0  	/*!< no log */
#define LOGLEVEL_ERROR		1	/*!< only log error messages */
#define LOGLEVEL_WARNING	2	/*!< log everything >= warning */
#define LOGLEVEL_INFO		3	/*!< log everything >= info */
#define LOGLEVEL_DEBUG		4	/*!< log everything */
#define LOGLEVEL_MAX		4	/*!< maximum log level */

/*! min() is not always defined, so do it here */
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })
   
#endif // #ifdef _LIBCONTRA_LOG_H_

/*! @} */

