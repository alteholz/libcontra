/* 
  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program (see the file COPYING included with this
  distribution); if not, write to the Free Software Foundation, Inc.,
  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*!
*************************************************************************
*
* \file libcontra_setsockopt.c
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \todo documentation
*
* \brief wrapper for setsockopt()
*
* set the socket options
* man 2 setsockopt
*
* \addtogroup libcontra
* The group of the library.
* @{
************************************************************************/

#include <libcontra.h>
#include <libcontra_log.h>

extern bool	libcontra_logSetsockopt; /*!< this is needed to control the output in the logging function */
extern bool	libcontra_isInitialized; /*!< this is needed to control the output in the logging function */
/*!
*************************************************************************
*
* \fn int setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len)
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper around libc::setsockopt()
*
* @param[in]  socket        set option for this socket
* @param[in]  level         protocol level at which the option resides
* @param[in]  option_name   set value for this option
* @param[out] option_value  new value of option
* @param[out] option_len    len of new value
* 
* \return On success, return 0
*         On error, return -1, errno is set
*
************************************************************************/
int setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len)
{
  int (*libc_setsockopt)(int socket, int level, int option_name, const void *option_value, socklen_t option_len);
  int rc;
  
  if (libcontra_isInitialized==false) {
    libcontra_init();
  } 

  if (libcontra_logSetsockopt) {
    libcontra_log(LOGLEVEL_DEBUG, "D: setsockopt start");
    libcontra_log(LOGLEVEL_INFO,  "I: setsockopt on socket v(%i) with level of %i - %i %s v(%i)",socket, level, option_name, libcontra_oname2string(option_name), option_len);
  }

  *(void **)(&libc_setsockopt) = dlsym(RTLD_NEXT, "setsockopt");
  if(dlerror()) {
    errno = EACCES;
    return -1;
  }

  rc = (*libc_setsockopt)(socket, level, option_name, option_value, option_len);
  
  if (libcontra_logSetsockopt) {
    libcontra_log(LOGLEVEL_INFO, "I: setsockopt rc: %i", rc);
    libcontra_log(LOGLEVEL_DEBUG, "D: setsockopt end");
  }

  return rc;
}

/*! @} */

