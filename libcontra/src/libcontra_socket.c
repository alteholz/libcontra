/* 
  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program (see the file COPYING included with this
  distribution); if not, write to the Free Software Foundation, Inc.,
  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*!
*************************************************************************
*
* \file libcontra_socket.c
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper for socket()
*
* create an endpoint for communication
* man 2 socket
*
* \addtogroup libcontra
* The group of the library.
* @{
************************************************************************/

#include <libcontra.h>
#include <libcontra_log.h>
#include <netdb.h>

extern bool	libcontra_logSocket; /*!< this is needed to control the output in the logging function */
extern bool	libcontra_isInitialized; /*!< this is needed to control the output in the logging function */

/*!
*************************************************************************
*
* \fn int socket(int family, int type, int protocol)
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \brief wrapper around libc::socket()
*
* @param[in]  family    specifies the family in which the socket is created
* @param[in]  type      type of socket
* @param[in]  protocol  protocol to be used with socket
*
* \return On success, return a non zero file descriptor for the socket
*         On error, return -1, errno is set
* 
************************************************************************/
int socket(int family, int type, int protocol)
{
  int (*libc_socket)(int family, int type, int protocol);
  int allocatedSocket;

  if (libcontra_isInitialized==false) {
    libcontra_init();
  } 

  if (libcontra_logSocket) {
    struct protoent *protocolInfo;
    protocolInfo=getprotobynumber(protocol); //beware, getprotobynumber() is not thread safe
    libcontra_log(LOGLEVEL_DEBUG, "D: socket start");
    libcontra_log(LOGLEVEL_INFO,  "I: family: \"%s\" -> type: \"%s\" -> protocol: %s",libcontra_af2string(family), libcontra_sock2string(type), protocolInfo->p_name);    
  }

  *(void **)(&libc_socket) = dlsym(RTLD_NEXT, "socket");
  if(dlerror()) {
    errno = EACCES;
    return -1;
  }

  allocatedSocket = (*libc_socket)(family, type, protocol);
  
  if (libcontra_logSocket) {
    libcontra_log(LOGLEVEL_INFO, "I: socket allocated: v(%i)", allocatedSocket);
    libcontra_log(LOGLEVEL_DEBUG, "D: socket end");
  }

  return allocatedSocket;
}

/*! @} */

