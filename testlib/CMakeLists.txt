#
#  libcontra - LIBrary for CONnection TRAcking
#
#  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program (see the file COPYING included with this
#  distribution); if not, write to the Free Software Foundation, Inc.,
#  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
cmake_minimum_required(VERSION 3.18)
project(ConTra)

add_test(NAME contra_test_start COMMAND echo "start overall tests")
add_test(NAME contra_test_pwd COMMAND ../../testlib/test_pwd_script) # just check whether tests are really running
add_test(NAME contra_test_server COMMAND ./test_server)              # direct call of test_server in order to see whether is was built
add_test(NAME contra_test_00 COMMAND ../../testlib/test_server_00)   # testcase 00
add_test(NAME contra_test_01 COMMAND ../../testlib/test_server_01)   # testcase 01, socket()
add_test(NAME contra_test_02 COMMAND ../../testlib/test_server_02)   # testcase 02, bind()
add_test(NAME contra_test_03 COMMAND ../../testlib/test_server_03)   # testcase 03, listen()
add_test(NAME contra_test_04 COMMAND ../../testlib/test_server_04)   # testcase 04, accept() + connect()
add_test(NAME contra_test_05 COMMAND ../../testlib/test_server_05)   # testcase 05, recv() + send()
add_test(NAME contra_test_06 COMMAND ../../testlib/test_server_06)   # testcase 06, setsockopt() + getsockopt
add_test(NAME contra_test_07 COMMAND ../../testlib/test_server_07)   # testcase 07, gethostbyname() + gethostbyaddr()
add_test(NAME contra_test_08 COMMAND ../../testlib/test_server_08)   # testcase 08, freeaddrinfo() + getaddrinfo()
add_test(NAME contra_test_end COMMAND echo "end overall tests")

add_executable(test_server test_server.c ../libcontra/src/libcontra_log.c)
TARGET_INCLUDE_DIRECTORIES(test_server PUBLIC ../libcontra/src)

add_executable(test_client test_client.c ../libcontra/src/libcontra_log.c)
TARGET_INCLUDE_DIRECTORIES(test_client PUBLIC ../libcontra/src)
