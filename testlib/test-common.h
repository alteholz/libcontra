/* 
  Copyright 2018-2023 Thorsten Alteholz <libcontra@alteholz.eu>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program (see the file COPYING included with this
  distribution); if not, write to the Free Software Foundation, Inc.,
  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*!
*************************************************************************
*
* \file test-common.h
*
* \author Thorsten Alteholz <libcontra@alteholz.eu>
*
* \todo documentation
*  the following wrapper functions need to be implemented:
*    send(), recv(), sendto(), and recvfrom()
*    gethostbyname() and gethostbyaddr()
*    getaddrinfo() and freeaddrinfo()
*    select()
*    getsockopt()
*    setsockopt()
*
* \brief header file for common stuff for test_client.c and test_server.c
*
* for more info see documents in docu directory
*
*
* \addtogroup libcontratest
* The group of the library.
* @{
************************************************************************/

#ifndef _TEST_COMMON_H_
#define _TEST_COMMON_H_

#define TEST_RECV_BUFFER_LEN 80 /*!< reserve 80 bytes in buffer */

#define TEST_SEND_MESSAGE "0123456789012345678901234567890123" /*!< test message to be sent */
#define TEST_SEND_MESSAGE_LEN 34 /*!< lenght of test message to be sent */

#endif
/*! @} */

